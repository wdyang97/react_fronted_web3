import { StaticJsonRpcProvider } from "@ethersproject/providers";
import { ethers } from "ethers";
import { getAddresses } from "../constants/addresses";
import { LMDTokenContract } from "../abi";
import { Networks } from "../constants/blockchain";


export async function transfer(networkID: Networks, provider: StaticJsonRpcProvider) {
    const addresses = getAddresses(networkID);
    const valueToSend = ethers.utils.parseUnits("10".toString(), 18);
    const lmdContract = new ethers.Contract(addresses.LMD_ADDRESS, LMDTokenContract, provider);
    const transaction = await lmdContract.transfer('0xB0202e25575FD23b22ABb49B3F9f5C62360815f0', valueToSend);
}