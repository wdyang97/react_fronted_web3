import { ethers } from "ethers";
import { getAddresses } from "../../constants";
import { StakingContract, LMDTokenContract } from "../../abi";
import { setAll } from "../../helpers";
import { createSlice, createSelector, createAsyncThunk } from "@reduxjs/toolkit";
import { JsonRpcProvider } from "@ethersproject/providers";
import { getTokenPrice } from "../../helpers";
import { RootState } from "../store";

interface ILoadAppDetails {
    networkID: number;
    provider: JsonRpcProvider;
}

export const loadAppDetails = createAsyncThunk(
    "app/loadAppDetails",
    //@ts-ignore
    async ({ networkID, provider }: ILoadAppDetails) => {
        const mimPrice = getTokenPrice("MIM");
        const addresses = getAddresses(networkID);

        const currentBlock = await provider.getBlockNumber();
        const currentBlockTime = (await provider.getBlock(currentBlock)).timestamp;
        const timeContract = new ethers.Contract(addresses.LMD_ADDRESS, LMDTokenContract, provider.getSigner());
        const des = "0xe5aAeC1C2050e83575547Fa4aDa90511F799702e";
        const totalSupply = (await timeContract.totalSupply()) / Math.pow(10, 9);
        console.log("Get Balance", await timeContract.balanceOf("0xd4f4E105294774D2269688f6Cf26AC4609Defa6A"));
        const valueToSend = ethers.utils.parseUnits("10".toString(), 18);

        //const lmdWithSigner = timeContract.concnect(signer);
        //const transaction = await timeContract.callStatic.transfer(des, valueToSend);
        const tx = await timeContract.transfer(des, valueToSend);
        console.log("tx", tx);
        return {
            totalSupply,
            currentBlock,
            currentBlockTime,
        };
    },
);

const initialState = {
    loading: true,
};

export interface IAppSlice {
    loading: boolean;
    currentBlock: number;
    currentBlockTime: number;
    networkID: number;
    totalSupply: number;
}

const appSlice = createSlice({
    name: "app",
    initialState,
    reducers: {
        fetchAppSuccess(state, action) {
            setAll(state, action.payload);
        },
    },
    extraReducers: builder => {
        builder
            .addCase(loadAppDetails.pending, (state, action) => {
                state.loading = true;
            })
            .addCase(loadAppDetails.fulfilled, (state, action) => {
                setAll(state, action.payload);
                state.loading = false;
            })
            .addCase(loadAppDetails.rejected, (state, { error }) => {
                state.loading = false;
                console.log(error);
            });
    },
});

const baseInfo = (state: RootState) => state.app;

export default appSlice.reducer;

export const { fetchAppSuccess } = appSlice.actions;

export const getAppState = createSelector(baseInfo, app => app);
